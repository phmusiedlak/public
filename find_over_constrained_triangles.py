# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 14:38:27 2023
"""

import numpy as np 
from data_manip.extraction.telemac_file import TelemacFile
import matplotlib.pyplot as plt
import matplotlib.tri as mtri

simu_folder = "folder_name/"
mesh_file = "mesh_file_name.slf"

res = TelemacFile( simu_folder + mesh_file )
telemac_mesh = {}
telemac_mesh["mesh"] = np.vstack((res.meshx , res.meshy)).T
telemac_mesh["ikle"] = res.ikle2

triang = mtri.Triangulation( telemac_mesh["mesh"][:,0] , telemac_mesh["mesh"][:,1] , triangles = telemac_mesh["ikle"] ) 


ind0 = np.where( triang.neighbors[:,0] == -1 )
ind1 = np.where( triang.neighbors[:,1] == -1 )
ind2 = np.where( triang.neighbors[:,2] == -1 )

temp = np.hstack((ind0,ind1,ind2))

ind3 , counts = np.unique(temp, return_counts=True )

ind4 = ind3[counts!=1] 
the_triangles = triang.triangles[ind4]
aa = the_triangles.flatten()

xy_coord =  np.vstack( (telemac_mesh["mesh"][aa,0] , telemac_mesh["mesh"][aa,1] )).T


plt.triplot(triang, 'b-', lw=0.1)
plt.plot( telemac_mesh["mesh"][aa,0] , telemac_mesh["mesh"][aa,1] , 'r.',ms=10)

np.savetxt( simu_folder + "over_constrained_triangles.xyz" , xy_coord )
