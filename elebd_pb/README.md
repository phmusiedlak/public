Author : Pierre-Henri Musiedlak
Date: 22/09/2023

Error: when LECLIM > INBIEF > ELEBF : K2=2147483647 
Error output: 


	INITIALIZING TELEMAC2D FOR
	 CALLING LECLIM
	 BACK FROM LECLIM
	 CALLING INBIEF
	 K2=  2147483647  K1=         374

	Program received signal SIGSEGV: Segmentation fault - invalid memory reference.


# Replicate the error

## Generate the bathy.slf and BC.cli

In BK, generate a mesh suing `T3 Mesh Generator` with area_withcoast.i2s as outline. 

Remove the nodes belonging to two elements (otherwise you will get another error), cells alike :
![BlueKenue19.png](BlueKenue19.png)

Generate an arbitrary bathymetry file : `File > New Selafin Object > Right click on it > Add Variable > BOTTOM` .
Save it under bathy.slf

Generate the .cli file: `File > New Boundary condition (conlim)... > select BOTTOM`
Specify one arbitrary west and east boundaries conditions (impose U,V,H with free tracer).
![BC.png](BC.png)


## Run the case

`telemac2d.py tel.cas`

should crash 

# Correct the mesh to run the case

One should can spot elements duplicate by inspecting visually the boundary condition mesh. Duplicates elements appear (at least for my case) on the other side of boundary:

![Duplicate_elements_2.png](Duplicate_elements_2.png)

And when selected from the original mesh they appear in BK as "Duplicates":

![Duplicate_elements.png](Duplicate_elements.png)

To solve the pb and run the case, inspect the whole boundary and remove (manually) these elements. Then, regenerate the bathy.slf and BC.cli. Cas should now run. 




# Procedure for meshing

## Generate the merge of Outline and coastline
 
Open files : area.i2s, coast_only.i2s, and view them in 2D view

Extract the french and uk coastlines of interest:
- Double-click on line and split it at point closest to area boundary (`right click > split selected line`). Then remove the bit you don't want (ie outside area, `right-click > cut`)
- repeat this until you only have only the bits of coastlines within the area: here one bit is the french coast, the other is the UK one.
- Create a random new closed line
- Copy the bit of UK coast into it (`right-click > Copy`, then select the newClosedLine `Edit > Line Set > Paste`)
- Add the french bit by coping it and then appending to the UK one (`right-click > Append`)
- Remove the aribtrary closedLine you made (`right-click > Cut`)
- Close the UK+frenc coast (`right-click > Close slected line`)

You should result in something alike: area_withcoast.i2s 

## Generate the mesh in BK
 
Using T3 mesh generator, create a mesh with default edge length 2500 using the fr_uk_channel closedLine as `Outline`.
 
Remove the nodes belonging to two elements (otherwise you will get another error). 

If you are lucky you might not have any "duplicates"...