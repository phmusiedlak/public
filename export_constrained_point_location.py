import numpy as np
from data_manip.extraction.telemac_file import TelemacFile


# find the overconstrained points in mesh from log
# log file is the output file from a 1time-step telemac2d simulation which `CHECKING THE MESH=YES`

# extract nodes numbers from log file
log_file = "log"
over_nodes = []
with open(log_file) as file:
    for line in file:
        if 'NEIGHBOURS' in line:
            over_nodes.append(int(line[line.find("POINT")+5:line.find("HAS")]))

print(over_nodes)

mesh_file = "bathy_2500_ref312.slf"
res = TelemacFile(mesh_file)
mesh = np.vstack((res.meshx , res.meshy)).T
# ikle = res.ikle2
res.close()

# print(len(mesh))
# print(mesh)
print(np.array(over_nodes)-1)
#print(mesh[np.array(over_nodes)-1])

np.savetxt("over_constrained_nodes_loc.xyz", mesh[np.array(over_nodes)-1] ,  delimiter=' ')

