# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 13:45:02 2023

Sometimes BK split the nodes list in multiple in the .cli
This creates pb like for using the Harmonic constants file

@author: pmusiedlak
"""

import numpy as np
import pandas as pd


simu_folder = "//wsl.localhost/Ubuntu-22.04/home/pmusiedlak/run_telemac/previmer_vJerome/"
bc_file = "Manche_3.cli"


table_bc = np.loadtxt( simu_folder + bc_file ) 
names = pd.read_csv( simu_folder+bc_file , usecols=[1] , sep = "#", header=None) 

ind = np.where(table_bc[:,0:3] == [5 , 6 ,6 ])
ind1 = np.unique(ind[0]) 
nb_of_bc_nodes = len(ind1)
bc_names = np.unique(names.iloc[ ind1 , :])
nb_bc = len(bc_names)


diff = np.diff(ind1)
if np.count_nonzero(diff-1) >= nb_bc :
    # BK has split the indexing of a BC into 2 or more
    # ==> need reindexing
    temp = table_bc[ np.where(names.iloc[:,0] == bc_names[0])[0]]
    table = pd.DataFrame(temp)
    table[len(table.iloc[0,:])] = len(temp)*[bc_names[0]] # add BC names
    for i in range(1,len(bc_names)):
        a = table_bc[ np.where(names.iloc[:,0] == bc_names[i])[0] ]
        # np.append(temp,a,0)
        df_a = pd.DataFrame( a ) 
        df_a[len(a[0,:])] = len(a)*[bc_names[i]]
        table = pd.concat([table , df_a ])
    # add rest of boundaries
    temp = table_bc[ np.where( names.iloc[:,0] == " ")]
    df_ = pd.DataFrame(temp)
    df_[len(df_.iloc[0,:])] = len(temp)*[" "]
    table = pd.concat([ table, df_  ] )
    table.iloc[:,12] = np.arange(1,len(table)+1)
    table.iloc[:,-1] = "#" + table.iloc[:,-1]
    # overwrites .cli
    table.to_csv(simu_folder+bc_file, sep= ' ', header = None, index= False , float_format="%d", quotechar=" " )
    
