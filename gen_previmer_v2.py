# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 09:21:15 2023

@author: pmusiedlak
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# generate the PREVIMER
from scipy.io import netcdf
from data_manip.extraction.telemac_file import TelemacFile
from pyproj import CRS, Transformer
import os


folder = "Q:/_InSitu_/Hindcast/MANGA/"

xe_uv = ["XE","U","V"]
use_only = True
user_specified_components = ["M2"]

# components available for the three XE, U and V
# components =  ["M2","N2","2N2","K1","K2","M2","M4","M6","MF","MK4","Mm","MN4","MS4","N2","O1","P1","Q1","S2"]


# load all files from MANGA folder
# and make a list for each XE, U and V available compoenents
cases_dir = []
filesandir = os.listdir(folder)

# load .netCFD files
data = {}
components = {i:[] for i in xe_uv} 
k=0
for file in filesandir : 
    onde = file[:file.find("-")]
    if use_only and (onde not in user_specified_components) : # to only load specific components
        continue # if condition is meet, following script is NOT done
    # netCFD often have a scale factor => maskandscale=True
    file2read = netcdf.NetCDFFile( folder + file ,'r',maskandscale=True)
    # U_a : amplitude
    # U_G : phase
    # mesh is [ x=longitude_u , y=latitude_u] in lat,long °
    var = file2read.variables
    for key in var :
        if k==0 : 
            if "longitude" in key  :
                temp = file2read.variables[key] 
                data["longitude"] = temp[:]*1
            if "latitude" in key :
                temp = file2read.variables[key] 
                data["latitude"] = temp[:]*1    
        if ("_a" in key) or ("_G" in key):
            temp = file2read.variables[key] 
            data[onde + "_" + key] = temp[:]*1
        if "_a" in key :
            components[key[:key.find('_')]].append(onde)
    file2read.close()       
    k=k+1 
    
    
#%% Plots

plot_on = False

if plot_on : 
    for xuv in components :
        for onde in components[xuv] :
            fig,ax = plt.subplots()
            ax.scatter( data["longitude"] , data["latitude"] , 
                       c = data[ onde +"_" + xuv + "_a"] , s=1 , cmap = 'jet')
            fig.colorbar( plt.cm.ScalarMappable(cmap = 'jet' ) ) 
            ax.set_title( xuv + " amplitude of onde " + onde )
            ax.set_xlabel("Longitude in ° (WGS-84)")
            ax.set_ylabel("Latitude in ° (WGS-84)")
            ax.grid(True)


#%% TELEMAC simulation         
    
simu_folder = "//wsl.localhost/Ubuntu-22.04/home/pmusiedlak/run_telemac/previmer/"
bc_file = "BOTTOM_BC_2500_ref312_HardPoints_2.cli"
simu_file = "bathy_2500_ref312_HardPoints.slf"

table_bc = np.loadtxt( simu_folder + bc_file , dtype = int) 
names = pd.read_csv( simu_folder+bc_file , usecols=[1] , sep = "#", header=None) 

ind = np.where(table_bc[:,0:3] == [5 , 6 ,6 ])
ind1 = np.unique(ind[0]) 
nb_of_bc_nodes = len(ind1)
bc_names = np.unique(names.iloc[ ind1 , :])
nb_bc = len(bc_names)


bc_nodes = {}
for i in bc_names :
      ind2 = np.where( names.iloc[:,-1]==i )
      bc_nodes[i] = table_bc[ind2[0],12]
      bc_nodes[i+"_mesh"] = table_bc[ind2[0],11]

# Load telemac bathy/mesh
res = TelemacFile( simu_folder + simu_file )
telemac_mesh = {}
telemac_mesh["mesh"] = np.vstack((res.meshx , res.meshy)).T

xy_nodes = {}
for i in bc_names :
    xy_nodes[i] = telemac_mesh["mesh"][bc_nodes[i+"_mesh"]-1] # because Python scrats at 0

if plot_on :
    fig,ax = plt.subplots()
    ax.plot( telemac_mesh["mesh"][:,0], telemac_mesh["mesh"][:,1] ,'.')
    for bc in bc_names :
        ax.plot( xy_nodes[bc][:,0] , xy_nodes[bc][:,1] ,'.',label=bc)
    ax.legend()

#%%        

# transform nodes location TO longitude/latitude as the NetCDF
epsg_to = "EPSG:4326"
epsg_from = "EPSG:32630" 
trans = Transformer.from_crs( epsg_from , epsg_to , always_xy=True)
inv_trans = Transformer.from_crs( epsg_to , epsg_from , always_xy=True)

# transform simu in WGS_84[lat/long] <=> EPSG:4326
fig,ax = plt.subplots()
ax.scatter( data["longitude"] , data["latitude"] , c = data["M2_U_a"] , s=1 )
for i in bc_names :
    if epsg_to == epsg_from :
        xy_nodes[i + "_4326"] = xy_nodes[i ] 
    else:     
        xy_nodes[i + "_4326"] = np.zeros(( len(xy_nodes[i]),2 ))
        for n in range(len(xy_nodes[i])) :
            xy_nodes[i + "_4326"][n,:] = trans.transform( xy_nodes[i][n,0] , xy_nodes[i][n,1] )
    ax.plot( xy_nodes[i + "_4326"][:,0] , xy_nodes[i + "_4326"][:,1] )

# # find the nodes of MANGA surrounding the bc_node (4 assuming its a rectangular mesh ?)
        
line = str(nb_bc) + "\n"
for bc in bc_names :
    line = line + str(bc_nodes[bc][0]) + "\t" + str(bc_nodes[bc][-1])  + "\n" # the node number in the BC description last column of .cli
line = line + str(nb_of_bc_nodes) + " " + str(len(components['XE'])) + " M \n"  
for onde in components['XE'] :
    line = line + onde + "\n"
    newline =''
    for i in bc_names :
        for n in range(len(xy_nodes[i])) :
            newline = newline + '\t' + str(xy_nodes[i+"_4326"][n,0]) + '\t' + str(xy_nodes[i+"_4326"][n,1]) 
            #bc_node is inside this square
            xmin = np.max(np.where(data["longitude"][0,:]  - xy_nodes[i+"_4326"][n,0] < 0 ))
            xmax = np.min(np.where( data["longitude"][0,:] - xy_nodes[i+"_4326"][n,0] > 0 ))
            ymin = np.max(np.where(data["latitude"][:,0]  - xy_nodes[i+"_4326"][n,1] < 0 ))
            ymax = np.min(np.where( data["latitude"][:,0] - xy_nodes[i+"_4326"][n,1] > 0 ))
            
            square = np.array(( [ data["longitude"][0,xmin], data["latitude"][ymin,0] ],
                               [  data["longitude"][0,xmax], data["latitude"][ymin,0] ] ,
                               [ data["longitude"][0,xmax], data["latitude"][ymax,0] ] ,
                               [  data["longitude"][0,xmin], data["latitude"][ymax,0] ]))
            s_trans = inv_trans.transform( square[:,0] , square[:,1] )
            d = np.sqrt( (xy_nodes[i][n,0] - s_trans[0] )**2 + (xy_nodes[i][n,1] - s_trans[1] )**2 )    
            
            for k in xe_uv :
                for amp_phase in ["_a","_G"] :
                    # !!! 
                    if onde in components[k] :
                        values = np.array(( data[onde + "_"+ k + amp_phase][ymin,xmin] , data[onde + "_"+ k + amp_phase][ymin,xmax], 
                                       data[onde + "_" + k + amp_phase][ymax,xmax] , data[onde + "_"+ k + amp_phase][ymax,xmin]  )) 
                    else:
                        values = np.zeros(4)
                    # weighted average value
                    value = np.average( values , weights = d )
                    if np.isnan(value): # amp_phase=="_a":
                        value = 0.0 # value/1000
                    # if k!="XE" and amp_phase=="_a":
                    #     value = 0.0
                    if abs(value)>1e5:
                        value=0.0
                    newline = newline + '\t' + str(value)
            newline = newline + "\n"
    line = line + newline                     
np.savetxt(simu_folder+"manga_harmonics.txt", [], header=line , comments='')       
        


